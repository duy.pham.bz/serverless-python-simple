# serverless-python-simple

Researching & Testing Serverless by Python programming language and return to the browser a simple message "Hello World!".

# Serverless Offline

First of all, you should run `npm i` to install necessary packages to run this project.

After that, you can run this command line to run this project on serverless:

```
    serverless offline
```

This is a result after that you run command line above:

![img](./images/result_command_line.png)

And when you click the URL `GET | http://localhost:3000/dev/hello`, your browser will open and access this URL. And this is a result of this project:

![img](./images/final.png)
