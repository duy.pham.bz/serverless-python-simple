import json


def hello(event, context):

    response = {
        "body": json.dumps({
            "message": "Hello World!"
        })
    }

    return response
